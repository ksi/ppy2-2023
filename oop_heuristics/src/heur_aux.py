import numpy as np


def is_integer(a):
    """
    Tests if `a` is integer
    """
    dt = a.dtype
    return dt == np.int16 or dt == np.int32 or dt == np.int64
