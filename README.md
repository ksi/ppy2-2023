# PPY2 – Programování v Pythonu 2

[![Static Badge](https://img.shields.io/badge/Synchronizovat%20na%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jupyter.fjfi.cvut.cz/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fppy2-2023&urlpath=lab%2Ftree%2Fppy2-2023%2Flpqp%2F01_intro.ipynb&branch=main)

## Odkazy

- [Bílá kniha](https://bilakniha.cvut.cz/cs/predmet7675306.html)
- [GitLab projekt](https://gitlab.fjfi.cvut.cz/ksi/ppy2-2023)
- [Informace o uživatelském účtu FJFI](https://it.fjfi.cvut.cz/component/flexicontent/clanky/uzivatelsky-ucet)
- [Fakultní JupyterHub](https://jupyter.fjfi.cvut.cz/)

## Podmínky pro zápočet z předmětu 18PPY2

- aktivní zapojení v praktické části (např. dotaz v diskuzi)
- <s>potřeba alespoň 70% bodů (8 z 11 dvouhodinovek)</s>
- potřeba **docházka** - alespoň 8 z 11 dvouhodinovek
- potřeba **aktivita** - alespoň 5 z 11 dvouhodinovek

Aktuální stav své aktivity a docházky můžete sledovat na [JupyterHubu](https://jupyter.fjfi.cvut.cz/) v souboru `18PPY2_dochazka.md` ve svém domovském adresáři.

## Harmonogram

1. Pátek  6. října      **Jakub Klinkovský (ČVUT-FJFI-KSI)** – Propojení jazyka Python s jazykem C++
2. Pátek 13. října      **Matej Mojzeš (ČVUT-FJFI-KSI)** – Využití zásad objektového programování při heuristické optimalizaci
3. Pátek 20. října      **Petr Kubera (ČVUT-FJFI-KSI)** – Úvod do strojového učení (základní pojmy, představení knihovny Scikit-Learn)
4. Pátek 27. října      **Petr Kubera (ČVUT-FJFI-KSI)** – Základy tvorby neuronových sítí s využitím knihovny Keras
5. Pátek  3. listopadu  **Čeněk Škarda (Cisco)** – Zpracování a statistická analýza velkých dat v distribuovaných systémech
6. Pátek 10. listopadu  **Lukáš Bátrla (Cisco)** – Využití strojového učení v praxi: od statistických modelů k umělé inteligenci
7. Pátek 24. listopadu  **Roman Sushkov (Amp X)** – [Integrace natrénovaných modelů v produkčních systémech a reálných aplikacích](https://github.com/sushkov-r/prod-demo)
8. Pátek  1. prosince   **Adam Peichl (ČVUT-FS)** – Praktické příklady použití lineárního a kvadratického programování se zaměřením na ekonomickou optimalizaci energetického zásobníku
9. Pátek  8. prosince   **Matej Mojzeš (ČVUT-FJFI-KSI)** – Využití prostředí Jupyter pro výzkumnou práci při respektování zásad softwarového vývoje, včetně verzování a “code review”
10. Pátek 15. prosince  **Adam Peichl (ČVUT-FS)** – Praktické příklady z teorie řízení (návrh jednoduchého řízení pro vodní turbínu, frekvenční charakteristika systému, stabilita)
11. Pátek 22. prosince  **děkanské volno**

Pokud vedení fakulty vyhlásí náhradní termín za pátek 22. prosince ještě před Vánoci, pokusíme se uspořádat i seminář na poslední slíbené téma:

- **Matej Mojzeš (ČVUT-FJFI-KSI)** – Využití Pythonu pro zpracování dat v cloudové službě Amazon Web Services
