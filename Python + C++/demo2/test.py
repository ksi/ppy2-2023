#! /usr/bin/env python3

import demo

#help(demo)
#help(demo.factorial)

assert demo.factorial(0) == 1
assert demo.factorial(1) == 1
assert demo.factorial(2) == 2
assert demo.factorial(6) == 720

demo.factorial(-1)
