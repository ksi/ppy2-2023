#include <stdexcept>

#include "mysource.h"

// This is the function that we want to bind to Python
long factorial(long n) {
    if (n < 0)
        throw std::invalid_argument("Factorial of a negative value " + std::to_string(n) + " is not defined.");

    long result = 1;
    while (n > 0) {
        result *= n--;
    }
    return result;
}

// This is a definition of a global variable
long global_variable = 42;

// These are the definitions of Pet's member functions
Pet::Pet(const std::string& name) : name(name)
{}
void Pet::setName(const std::string& name)
{
    this->name = name;
}
const std::string& Pet::getName() const
{
    return name;
}