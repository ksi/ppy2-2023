#! /usr/bin/env python3

import demo
import advanced_demo

def callback(pet):
    print(f"Hello, {pet.getName()}! Python will now eat you...")
    pet.setName("no kitty")
    return 42

advanced_demo.call_python(callback)