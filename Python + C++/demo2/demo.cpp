#include "mysource.h"
#include <pybind11/pybind11.h>

namespace py = pybind11;

// Definition of the Python module
PYBIND11_MODULE(demo, m) {
    // set optional docstring of the module
    m.doc() = "This is the module docstring";

    // define members of the module
    m.def("factorial", &factorial, "A function that computes the factorial of an integer");

    // definition of a module attribute
    m.attr("global_variable") = &global_variable;

    // definition of a wrapper class for Pet
    py::class_<Pet>(m, "Pet")
        .def(py::init<const std::string&>())
        .def("setName", &Pet::setName)
        .def("getName", &Pet::getName);
}
