#!/usr/bin/env python
# coding: utf-8

# # About
# 
# In this notebook, we wan to explore a car mpg dataset and develop a model for power prediction.

# In[1]:


get_ipython().run_line_magic('load_ext', 'version_information')
get_ipython().run_line_magic('version_information', 'pandas, numpy, ydata_profiling, matplotlib, seaborn, statsmodels')


# In[2]:


import pandas as pd
import numpy as np


# In[3]:


from ydata_profiling import ProfileReport


# In[4]:


import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

get_ipython().run_line_magic('matplotlib', 'inline')


# In[5]:


import statsmodels.api as sm


# ## Car mpg data set
# 
# Downloaded from https://archive.ics.uci.edu/ml/datasets/Auto+MPG.
# 
# File preview (in bash):

# In[6]:


get_ipython().system('head auto-mpg.data.txt')


# Import the fixed width format:

# In[7]:


mpg = pd.read_fwf(
    'auto-mpg.data.txt', 
    na_values='?',  # see documentation
    header=0, 
    names=['mpg', 'cylinders', 'displacement', 'horsepower', 'weight', 'acceleration', 'model year', 'origin', 'car name'])                  
mpg.info()


# In[8]:


mpg.head()


# Mind the NA (NULL) values in the `horsepower` variable.

# This looks good, but we want to explore it better. Let's use ydata-profiling.

# In[9]:


profile = ProfileReport(mpg, title="Car MPG Profiling Report")


# In[10]:


profile


# ### Conclusion
# 
# The data seem to make sense, there seems to be good correlation between engine displacement and its power. Hence, I will proceed with the modelling using this data.

# ## Could we use displacement (in bins) to predict horsepower?
# 
# (Probably not the best idea, and the execution, but let's try...)

# Let's prepare the bins:

# In[11]:


bin_size = 50
disp_min = mpg['displacement'].min()
disp_max = mpg['displacement'].max()
bins = [disp_min + bin_size * s for s in np.arange(np.ceil((disp_max - disp_min) / bin_size))]


# In[12]:


bins


# **Mean horse power by displacement bins**:

# In[13]:


hp = mpg.groupby(np.digitize(mpg['displacement'], bins))['horsepower'].mean()
hp


# **Let's try ordinary least squares**:

# In[14]:


y = hp.values
y


# In[15]:


X = hp.index.values
X


# In[16]:


X = sm.add_constant(X)
X


# In[17]:


model = sm.OLS(y, X)
results = model.fit()


# In[18]:


results.params


# Fit plot:

# In[19]:


fig = sm.graphics.plot_fit(results, 'x1')
fig.tight_layout(pad=1.0)


# In[20]:


print(results.summary())


# See more on [OLS](http://www.statsmodels.org/dev/generated/statsmodels.regression.linear_model.OLS.html).

# ## Conclusion
# 
# This is a model for power prediction, which can be used to predict engine's power based on its displacement.
# 
# Caveat: it assumes only displacement in the range of [68, 418]. Use at your own risk.
